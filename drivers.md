├─hdf  
│  ├─frameworks  
│  │  ├─ability  
│  │  │  ├─config  
│  │  │  │  └─hcs_parser  
│  │  │  │      ├─include  
│  │  │  │      └─src  
│  │  │  └─sbuf  
│  │  │      ├─include  
│  │  │      └─src  
│  │  ├─core  
│  │  │  ├─host  
│  │  │  │  ├─include  
│  │  │  │  └─src  
│  │  │  ├─manager  
│  │  │  │  ├─include  
│  │  │  │  └─src  
│  │  │  └─shared  
│  │  │      ├─include  
│  │  │      └─src  
│  │  ├─include  
│  │  │  ├─config  
│  │  │  ├─core  
│  │  │  ├─net  
│  │  │  ├─osal  
│  │  │  ├─platform  
│  │  │  ├─utils  
│  │  │  └─wifi  
│  │  ├─model  
│  │  │  └─network  
│  │  │      └─wifi  
│  │  │          ├─compoments  
│  │  │          │  ├─eapol  
│  │  │          │  ├─softap  
│  │  │          │  └─sta  
│  │  │          ├─include  
│  │  │          ├─module  
│  │  │          └─netdevice  
│  │  ├─support  
│  │  │  └─platform  
│  │  │      ├─include  
│  │  │      └─src  
│  │  ├─tools  
│  │  │  └─hc-gen  
│  │  │      ├─include  
│  │  │      └─src  
│  │  └─utils  
│  │      ├─include  
│  │      └─src  
│  └─lite  
│      ├─adapter  
│      │  ├─network  
│      │  │  ├─include  
│      │  │  └─src  
│      │  ├─osal  
│      │  │  ├─posix  
│      │  │  │  └─src  
│      │  │  └─src  
│      │  ├─syscall  
│      │  │  ├─include  
│      │  │  └─src  
│      │  └─vnode  
│      │      ├─include  
│      │      └─src  
│      ├─hdi  
│      │  ├─audio  
│      │  │  └─include  
│      │  ├─codec  
│      │  │  └─include  
│      │  ├─display  
│      │  │  └─include  
│      │  ├─format  
│      │  │  └─include  
│      │  └─input  
│      │      └─include  
│      ├─include  
│      │  ├─host  
│      │  └─manager  
│      ├─manager  
│      │  └─src  
│      ├─model  
│      │  ├─bus  
│      │  │  └─usb  
│      │  │      └─include  
│      │  └─network  
│      │      └─wifi  
│      ├─posix  
│      └─tools  
│          └─hdf_dev_eco_tool  
│              ├─command_line  
│              └─resources  
│                  └─templates  
│                      └─lite  
└─liteos  
    ├─hievent  
    │  ├─include  
    │  └─src  
    ├─include  
    │  └─mtd  
    ├─mem  
    │  └─src  
    ├─random  
    │  ├─include  
    │  └─src  
    ├─tzdriver  
    │  ├─include  
    │  └─src  
    └─video  