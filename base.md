├─global  
│  ├─frameworks  
│  │  └─resmgr_lite  
│  │      ├─include  
│  │      └─src  
│  └─interfaces  
│      └─innerkits  
│          └─resmgr_lite  
├─hiviewdfx  
│  ├─frameworks  
│  │  ├─ddrdump_lite  
│  │  ├─hievent_lite  
│  │  └─hilog_lite  
│  │      ├─featured  
│  │      └─mini  
│  ├─interfaces  
│  │  ├─innerkits  
│  │  │  ├─hievent_lite  
│  │  │  └─hilog  
│  │  └─kits  
│  │      ├─hilog  
│  │      └─hilog_lite  
│  ├─services  
│  │  ├─hilogcat_lite  
│  │  │  ├─apphilogcat  
│  │  │  ├─command  
│  │  │  └─hilogcat  
│  │  └─hiview_lite  
│  └─utils  
│      └─lite  
├─iot_hardware  
│  ├─frameworks  
│  │  └─wifiiot_lite  
│  │      └─src  
│  ├─hals  
│  │  └─wifiiot_lite  
│  └─interfaces  
│      └─kits  
│          └─wifiiot_lite  
├─security  
│  ├─frameworks  
│  │  ├─app_verify  
│  │  │  ├─include  
│  │  │  ├─products  
│  │  │  │  ├─default  
│  │  │  │  └─ipcamera  
│  │  │  └─src  
│  │  ├─crypto_lite  
│  │  │  ├─cipher  
│  │  │  │  ├─include  
│  │  │  │  └─src  
│  │  │  └─js  
│  │  │      └─builtin  
│  │  │          ├─include  
│  │  │          └─src  
│  │  ├─hichainsdk_lite  
│  │  │  └─source  
│  │  │      ├─auth_info  
│  │  │      ├─base  
│  │  │      │  └─product_header  
│  │  │      ├─huks_adapter  
│  │  │      ├─json  
│  │  │      ├─key_agreement  
│  │  │      ├─log  
│  │  │      ├─schedule  
│  │  │      └─struct  
│  │  ├─huks_lite  
│  │  │  └─source  
│  │  │      ├─base  
│  │  │      └─hw_keystore_sdk  
│  │  │          ├─common  
│  │  │          └─soft_service  
│  │  └─secure_os  
│  │      └─libteec  
│  │          ├─include  
│  │          └─src  
│  ├─interfaces  
│  │  ├─innerkits  
│  │  │  ├─app_verify  
│  │  │  ├─crypto_lite  
│  │  │  ├─hichainsdk_lite  
│  │  │  ├─huks_lite  
│  │  │  ├─iam_lite  
│  │  │  └─secure_os  
│  │  │      └─libteec  
│  │  └─kits  
│  │      └─iam_lite  
│  └─services  
│      ├─iam_lite  
│      │  ├─ipc_auth  
│      │  │  ├─include  
│      │  │  └─src  
│      │  ├─js_api  
│      │  │  ├─include  
│      │  │  └─src  
│      │  ├─pms  
│      │  │  ├─include  
│      │  │  └─src  
│      │  ├─pms_base  
│      │  │  ├─include  
│      │  │  └─src  
│      │  └─pms_client  
│      └─secure_os  
│          └─teecd  
│              ├─include  
│              └─src  
└─startup  
    ├─frameworks  
    │  └─syspara_lite  
    │      ├─parameter  
    │      │  └─src  
    │      │      ├─param_impl_hal  
    │      │      └─param_impl_posix  
    │      └─token  
    │          └─src  
    │              ├─token_impl_hal  
    │              └─token_impl_posix  
    ├─hals  
    │  └─syspara_lite  
    ├─interfaces  
    │  └─kits  
    │      └─syspara_lite  
    └─services  
        ├─appspawn_lite  
        │  ├─include  
        │  └─src  
        ├─bootstrap_lite  
        │  └─source  
        └─init_lite  
            ├─include  
            └─src  
