├─camera  
│  ├─hals  
│  │  └─utils  
│  │      ├─sys_param  
│  │      └─token  
│  └─init_configs  
├─hdf  
│  ├─display  
│  │  ├─driver  
│  │  └─hdi  
│  │      └─hi35xx  
│  ├─input  
│  │  ├─driver  
│  │  └─hdi  
│  ├─libs  
│  │  ├─hi3516dv300  
│  │  └─hi3518ev300  
│  ├─sample  
│  │  ├─config  
│  │  │  ├─device_info  
│  │  │  └─uart  
│  │  └─platform  
│  │      └─uart  
│  │          ├─dev  
│  │          ├─dispatch  
│  │          ├─include  
│  │          └─src  
│  └─wifi  
│      └─driver  
│          └─firmware  
│              └─hisilicon  
└─wifi-iot  
    └─hals  
        └─utils  
            ├─sys_param  
            └─token  