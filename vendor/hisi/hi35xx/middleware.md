└─source  
    ├─common  
    │  ├─hitimer  
    │  │  └─lib  
    │  │      └─llvm  
    │  │          └─ohos  
    │  ├─log  
    │  │  └─lib  
    │  │      └─llvm  
    │  │          └─ohos  
    │  ├─mbuffer  
    │  │  └─lib  
    │  │      └─llvm  
    │  │          └─ohos  
    │  └─msghandler  
    │      └─lib  
    │          └─llvm  
    │              └─ohos  
    ├─component  
    │  ├─dtcf  
    │  │  └─lib  
    │  │      └─llvm  
    │  │          └─ohos  
    │  ├─fileformat  
    │  │  ├─common  
    │  │  │  └─lib  
    │  │  │      └─llvm  
    │  │  │          └─ohos  
    │  │  ├─mp4  
    │  │  │  └─lib  
    │  │  │      └─llvm  
    │  │  │          └─ohos  
    │  │  └─ts  
    │  │      └─lib  
    │  │          └─llvm  
    │  │              └─ohos  
    │  ├─fstool  
    │  │  └─lib  
    │  │      └─llvm  
    │  │          └─ohos  
    │  └─recorder_pro  
    │      └─lib  
    │          └─llvm  
    │              └─ohos  
    └─third_party  
        └─ffmpeg  
            └─ffmpeg-y  
                ├─compat  
                │  ├─aix  
                │  ├─atomics  
                │  │  ├─dummy  
                │  │  ├─gcc  
                │  │  ├─pthread  
                │  │  ├─suncc  
                │  │  └─win32  
                │  ├─avisynth  
                │  │  ├─avs  
                │  │  └─windowsPorts  
                │  ├─cuda  
                │  ├─dispatch_semaphore  
                │  ├─djgpp  
                │  ├─float  
                │  ├─msvcrt  
                │  ├─solaris  
                │  └─windows  
                ├─doc  
                │  ├─doxy  
                │  └─examples  
                ├─ffbuild  
                ├─fftools  
                ├─libavcodec  
                │  ├─aarch64  
                │  ├─alpha  
                │  ├─arm  
                │  ├─avr32  
                │  ├─bfin  
                │  ├─mips  
                │  ├─neon  
                │  ├─ppc  
                │  ├─sh4  
                │  ├─sparc  
                │  ├─tests  
                │  │  ├─aarch64  
                │  │  ├─arm  
                │  │  ├─ppc  
                │  │  └─x86  
                │  └─x86  
                ├─libavdevice  
                │  └─tests  
                ├─libavfilter  
                │  ├─aarch64  
                │  ├─opencl  
                │  ├─tests  
                │  └─x86  
                ├─libavformat  
                │  └─tests  
                ├─libavresample  
                │  ├─aarch64  
                │  ├─arm  
                │  ├─tests  
                │  └─x86  
                ├─libavutil  
                │  ├─aarch64  
                │  ├─arm  
                │  ├─avr32  
                │  ├─bfin  
                │  ├─mips  
                │  ├─ppc  
                │  ├─sh4  
                │  ├─tests  
                │  ├─tomi  
                │  └─x86  
                ├─libpostproc  
                ├─libswresample  
                │  ├─aarch64  
                │  ├─arm  
                │  ├─tests  
                │  └─x86  
                ├─libswscale  
                │  ├─aarch64  
                │  ├─arm  
                │  ├─ppc  
                │  ├─tests  
                │  └─x86  
                ├─presets  
                ├─tests  
                │  ├─api  
                │  ├─checkasm  
                │  │  ├─aarch64  
                │  │  ├─arm  
                │  │  └─x86  
                │  ├─fate  
                │  ├─filtergraphs  
                │  └─ref  
                │      ├─acodec  
                │      ├─fate  
                │      ├─lavf  
                │      ├─lavf-fate  
                │      ├─pixfmt  
                │      ├─seek  
                │      ├─vsynth  
                │      ├─vsynth1  
                │      └─vsynth_lena  
                └─tools  
                    └─python  
