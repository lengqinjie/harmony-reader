├─gpio  
├─hiedmac  
├─hieth-sf  
├─hisi_sdk  
├─i2c  
├─libs  
│  ├─hi3516dv300  
│  └─hi3518ev300  
├─mmc  
│  └─include  
│      └─mmc  
├─mtd  
│  ├─common  
│  │  └─include  
│  └─spi_nor  
├─rtc  
├─sdio  
├─spi  
├─uart  
└─watchdog  
