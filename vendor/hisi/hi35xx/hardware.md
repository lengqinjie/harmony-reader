└─media  
    └─hal  
        ├─audio  
        │  ├─hi3516dv300  
        │  │  └─llvm  
        │  │      └─ohos  
        │  │          └─libs  
        │  └─hi3518ev300  
        │      └─llvm  
        │          └─ohos  
        │              └─libs  
        ├─codec  
        │  ├─hi3516dv300  
        │  │  └─llvm  
        │  │      └─ohos  
        │  │          └─libs  
        │  └─hi3518ev300  
        │      └─llvm  
        │          └─ohos  
        │              └─libs  
        └─format  
            ├─hi3516dv300  
            │  └─llvm  
            │      └─ohos  
            │          └─libs  
            └─hi3518ev300  
                └─llvm  
                    └─ohos  
                        └─libs  