└─sample  
    ├─camera  
    │  ├─app  
    │  │  ├─include  
    │  │  └─src  
    │  ├─communication  
    │  │  ├─hostapd  
    │  │  │  ├─config  
    │  │  │  └─src  
    │  │  ├─wpa_cli  
    │  │  │  └─src  
    │  │  └─wpa_supplicant  
    │  │      ├─config  
    │  │      └─src  
    │  ├─example  
    │  │  ├─consumer  
    │  │  ├─intermediary  
    │  │  └─provider  
    │  ├─hap  
    │  ├─media  
    │  └─reader  
    │      ├─include  
    │      └─src  
    └─wifi-iot  
        └─app  
            ├─demolink  
            ├─iothardware  
            ├─samgr  
            └─startup  
