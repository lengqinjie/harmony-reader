├─arch  
│  ├─arm  
│  │  ├─cortex-a  
│  │  └─cortex-m  
│  │      ├─cortex-m4  
│  │      │  └─iar  
│  │      ├─include  
│  │      └─src  
│  └─risc-v  
├─components  
│  ├─cmsis  
│  │  └─2.0  
│  ├─cmsis_adp  
│  └─kal  
│      ├─include  
│      └─src  
├─kernel  
│  ├─base  
│  │  ├─core  
│  │  ├─include  
│  │  ├─ipc  
│  │  ├─mem  
│  │  │  ├─bestfit  
│  │  │  └─common  
│  │  ├─misc  
│  │  └─om  
│  ├─extended  
│  │  ├─cppsupport  
│  │  ├─cpup  
│  │  └─include  
│  └─include  
└─platform  
    └─cpu  
        └─arm  
            ├─cortex-m4  
            │  └─src  
            └─cortex-m7  
                └─src  
