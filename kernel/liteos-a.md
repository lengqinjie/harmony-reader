├─apps  
│  ├─init  
│  │  └─src  
│  └─shell  
│      ├─builtin  
│      ├─include  
│      └─src  
├─arch  
│  └─arm  
│      ├─arm  
│      │  ├─include  
│      │  └─src  
│      │      ├─armv7a  
│      │      ├─include  
│      │      └─startup  
│      └─include  
├─bsd  
│  ├─arm  
│  │  └─include  
│  ├─compat  
│  │  └─linuxkpi  
│  │      ├─include  
│  │      │  ├─asm  
│  │      │  └─linux  
│  │      │      └─mtd  
│  │      └─src  
│  ├─crypto  
│  │  ├─rijndael  
│  │  └─sha2  
│  ├─dev  
│  │  ├─random  
│  │  └─usb  
│  │      ├─controller  
│  │      ├─implementation  
│  │      ├─input  
│  │      ├─net  
│  │      ├─quirk  
│  │      ├─serial  
│  │      └─storage  
│  ├─kern  
│  ├─libkern  
│  ├─net  
│  └─sys  
├─compat  
│  └─posix  
│      ├─include  
│      └─src  
├─fs  
│  ├─fat  
│  │  ├─os_adapt  
│  │  └─virpart  
│  │      ├─include  
│  │      └─src  
│  ├─include  
│  │  ├─fs  
│  │  ├─inode  
│  │  └─net  
│  ├─jffs2  
│  ├─nfs  
│  ├─proc  
│  │  ├─include  
│  │  ├─os_adapt  
│  │  └─src  
│  ├─ramfs  
│  └─vfs  
│      ├─bcache  
│      │  └─src  
│      ├─bch  
│      ├─disk  
│      ├─include  
│      │  ├─bcache  
│      │  ├─disk  
│      │  ├─driver  
│      │  ├─multi_partition  
│      │  └─operation  
│      ├─multi_partition  
│      │  └─src  
│      ├─operation  
│      └─vfs_cmd  
├─kernel  
│  ├─base  
│  │  ├─core  
│  │  ├─include  
│  │  ├─ipc  
│  │  ├─mem  
│  │  │  ├─bestfit  
│  │  │  ├─bestfit_little  
│  │  │  ├─common  
│  │  │  │  └─memrecord  
│  │  │  └─membox  
│  │  ├─misc  
│  │  ├─mp  
│  │  ├─om  
│  │  ├─sched  
│  │  │  └─sched_sq  
│  │  └─vm  
│  ├─common  
│  ├─extended  
│  │  ├─cppsupport  
│  │  ├─cpup  
│  │  ├─dynload  
│  │  │  ├─include  
│  │  │  └─src  
│  │  ├─include  
│  │  ├─liteipc  
│  │  ├─tickless  
│  │  ├─trace  
│  │  └─vdso  
│  │      ├─include  
│  │      ├─src  
│  │      └─usr  
│  ├─include  
│  └─user  
│      ├─include  
│      └─src  
├─lib  
│  ├─libc  
│  ├─libmbedtls  
│  ├─libscrew  
│  │  ├─include  
│  │  └─src  
│  ├─libsec  
│  └─zlib  
├─net  
│  ├─lwip-2.1  
│  │  ├─enhancement  
│  │  │  ├─include  
│  │  │  │  └─lwip  
│  │  │  └─src  
│  │  └─porting  
│  │      ├─include  
│  │      │  ├─arch  
│  │      │  └─lwip  
│  │      │      └─priv  
│  │      └─src  
│  ├─mac  
│  └─telnet  
│      ├─include  
│      └─src  
├─platform  
│  ├─hw  
│  │  ├─arm  
│  │  │  ├─interrupt  
│  │  │  │  └─gic  
│  │  │  └─timer  
│  │  │      ├─arm_generic  
│  │  │      └─arm_private  
│  │  ├─hisoc  
│  │  │  ├─hrtimer  
│  │  │  └─timer  
│  │  └─include  
│  ├─include  
│  ├─uart  
│  │  ├─amba-pl011-lagacy  
│  │  ├─amba_pl011  
│  │  └─dw-3.0.8a  
│  └─usb  
│      ├─usb3.0_hi3516dv300  
│      └─usb3.0_hi3518ev300  
├─security  
│  ├─cap  
│  └─vid  
├─shell  
│  └─full  
│      ├─include  
│      └─src  
│          ├─base  
│          └─cmds  
├─syscall  
└─tools  
    ├─build  
    │  ├─config  
    │  │  └─debug  
    │  └─mk  
    ├─fsimage  
    ├─menuconfig  
    └─scripts  
        └─make_rootfs  
