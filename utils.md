└─native  
    └─lite  
        ├─file  
        │  └─src  
        │      └─file_impl_hal  
        ├─hals  
        │  └─file  
        ├─include  
        ├─js  
        │  └─builtin  
        │      ├─common  
        │      │  ├─include  
        │      │  └─src  
        │      ├─deviceinfokit  
        │      │  ├─include  
        │      │  └─src  
        │      ├─filekit  
        │      │  ├─include  
        │      │  └─src  
        │      └─kvstorekit  
        │          ├─include  
        │          └─src  
        ├─kal  
        │  └─timer  
        │      ├─include  
        │      └─src  
        ├─kv_store  
        │  ├─innerkits  
        │  └─src  
        │      ├─kvstore_common  
        │      ├─kvstore_impl_hal  
        │      └─kvstore_impl_posix  
        └─timer_task  
            ├─include  
            └─src  
