├─lib  
│  └─libc  
│      └─arm  
│          └─string  
├─stand  
│  ├─kshim  
│  └─usb  
└─sys  
    ├─arm  
    │  ├─arm  
    │  └─include  
    ├─cam  
    │  └─scsi  
    ├─compat  
    │  └─linuxkpi  
    │      └─common  
    │          ├─include  
    │          │  ├─asm  
    │          │  └─linux  
    │          └─src  
    ├─crypto  
    │  ├─rijndael  
    │  └─sha2  
    ├─dev  
    │  ├─mii  
    │  ├─random  
    │  └─usb  
    │      ├─controller  
    │      ├─implementation  
    │      ├─input  
    │      ├─net  
    │      ├─quirk  
    │      ├─serial  
    │      └─storage  
    ├─kern  
    ├─libkern  
    ├─net  
    └─sys  
