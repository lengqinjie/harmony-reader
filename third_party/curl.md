├─CMake  
│  └─Platforms  
├─docs  
│  ├─cmdline-opts  
│  ├─examples  
│  └─libcurl  
│      └─opts  
├─include  
│  └─curl  
├─lib  
│  ├─vauth  
│  ├─vquic  
│  ├─vssh  
│  └─vtls  
├─m4  
├─packages  
│  ├─Android  
│  ├─DOS  
│  ├─OS400  
│  ├─Symbian  
│  │  ├─bwins  
│  │  ├─eabi  
│  │  └─group  
│  ├─TPF  
│  └─vms  
├─plan9  
│  ├─include  
│  ├─lib  
│  └─src  
├─projects  
│  └─Windows  
│      ├─VC10  
│      │  ├─lib  
│      │  └─src  
│      ├─VC11  
│      │  ├─lib  
│      │  └─src  
│      ├─VC12  
│      │  ├─lib  
│      │  └─src  
│      ├─VC14  
│      │  ├─lib  
│      │  └─src  
│      ├─VC15  
│      │  ├─lib  
│      │  └─src  
│      ├─VC6  
│      │  ├─lib  
│      │  └─src  
│      ├─VC7  
│      │  ├─lib  
│      │  └─src  
│      ├─VC7.1  
│      │  ├─lib  
│      │  └─src  
│      ├─VC8  
│      │  ├─lib  
│      │  └─src  
│      └─VC9  
│          ├─lib  
│          └─src  
├─scripts  
│  └─travis  
├─src  
│  └─macos  
│      └─src  
├─tests  
│  ├─certs  
│  │  └─scripts  
│  ├─data  
│  ├─fuzz  
│  ├─libtest  
│  ├─python_dependencies  
│  │  └─impacket  
│  ├─server  
│  └─unit  
└─winbuild  