├─builds  
│  ├─amiga  
│  │  ├─include  
│  │  │  └─config  
│  │  └─src  
│  │      └─base  
│  ├─ansi  
│  ├─atari  
│  ├─beos  
│  ├─cmake  
│  ├─compiler  
│  ├─dos  
│  ├─mac  
│  ├─os2  
│  ├─symbian  
│  ├─unix  
│  ├─vms  
│  ├─wince  
│  │  ├─vc2005-ce  
│  │  └─vc2008-ce  
│  └─windows  
│      ├─vc2010  
│      ├─visualc  
│      └─visualce  
├─devel  
├─docs  
│  └─reference  
│      └─site  
│          ├─assets  
│          │  ├─fonts  
│          │  │  └─specimen  
│          │  ├─images  
│          │  │  └─icons  
│          │  ├─javascripts  
│          │  │  └─lunr  
│          │  └─stylesheets  
│          ├─images  
│          ├─javascripts  
│          ├─search  
│          └─stylesheets  
├─include  
│  └─freetype  
│      ├─config  
│      └─internal  
│          └─services  
├─objs  
└─src  
    ├─autofit  
    ├─base  
    ├─bdf  
    ├─bzip2  
    ├─cache  
    ├─cff  
    ├─cid  
    ├─gxvalid  
    ├─gzip  
    ├─lzw  
    ├─otvalid  
    ├─pcf  
    ├─pfr  
    ├─psaux  
    ├─pshinter  
    ├─psnames  
    ├─raster  
    ├─sfnt  
    ├─smooth  
    ├─tools  
    │  └─ftrandom  
    ├─truetype  
    ├─type1  
    ├─type42  
    └─winfonts  