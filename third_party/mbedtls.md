├─mbedtls  
│  ├─configs  
│  ├─doxygen  
│  │  └─input  
│  ├─include  
│  │  └─mbedtls  
│  ├─library  
│  ├─programs  
│  │  ├─aes  
│  │  ├─hash  
│  │  ├─pkey  
│  │  ├─random  
│  │  ├─ssl  
│  │  ├─test  
│  │  ├─util  
│  │  └─x509  
│  ├─scripts  
│  │  └─data_files  
│  ├─tests  
│  │  ├─.jenkins  
│  │  ├─configs  
│  │  ├─data_files  
│  │  │  ├─dir-maxpath  
│  │  │  ├─dir1  
│  │  │  ├─dir2  
│  │  │  ├─dir3  
│  │  │  └─dir4  
│  │  ├─git-scripts  
│  │  ├─scripts  
│  │  └─suites  
│  └─visualc  
│      └─VS2010  