├─Linux_Kernel  
│  ├─fs  
│  │  └─jffs2  
│  └─scripts  
│      ├─basic  
│      ├─coccinelle  
│      │  ├─api  
│      │  │  └─alloc  
│      │  ├─free  
│      │  ├─iterators  
│      │  ├─locks  
│      │  ├─misc  
│      │  ├─null  
│      │  └─tests  
│      ├─dtc  
│      │  └─libfdt  
│      ├─gdb  
│      │  └─linux  
│      ├─genksyms  
│      ├─kconfig  
│      │  └─lxdialog  
│      ├─ksymoops  
│      ├─mod  
│      ├─package  
│      ├─selinux  
│      │  ├─genheaders  
│      │  └─mdp  
│      └─tracing  