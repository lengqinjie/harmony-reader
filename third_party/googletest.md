├─ci  
├─googlemock  
│  ├─cmake  
│  ├─docs  
│  ├─include  
│  │  └─gmock  
│  │      └─internal  
│  │          └─custom  
│  ├─msvc  
│  │  ├─2005  
│  │  ├─2010  
│  │  └─2015  
│  ├─scripts  
│  │  └─generator  
│  │      └─cpp  
│  ├─src  
│  └─test  
└─googletest  
    ├─cmake  
    ├─codegear  
    ├─docs  
    ├─include  
    │  └─gtest  
    │      ├─hwext  
    │      └─internal  
    │          └─custom  
    ├─m4  
    ├─msvc  
    │  └─2010  
    ├─samples  
    ├─scripts  
    ├─src  
    │  └─hwext  
    ├─test  
    └─xcode  
        ├─Config  
        ├─gtest.xcodeproj  
        ├─Resources  
        ├─Samples  
        │  └─FrameworkSample  
        │      └─WidgetFramework.xcodeproj  
        └─Scripts  
