├─docs  
├─m4  
├─src  
├─test  
│  ├─api  
│  │  └─fonts  
│  ├─fuzzing  
│  │  └─fonts  
│  ├─shaping  
│  │  ├─data  
│  │  │  ├─aots  
│  │  │  │  ├─fonts  
│  │  │  │  └─tests  
│  │  │  ├─in-house  
│  │  │  │  ├─fonts  
│  │  │  │  └─tests  
│  │  │  └─text-rendering-tests  
│  │  │      ├─fonts  
│  │  │      └─tests  
│  │  └─texts  
│  │      └─in-house  
│  │          ├─shaper-arabic  
│  │          │  ├─script-arabic  
│  │          │  │  ├─language-persian  
│  │          │  │  ├─language-urdu  
│  │          │  │  │  └─crulp  
│  │          │  │  │      └─ligatures  
│  │          │  │  └─misc  
│  │          │  │      └─diacritics  
│  │          │  ├─script-mongolian  
│  │          │  │  └─misc  
│  │          │  ├─script-nko  
│  │          │  │  └─misc  
│  │          │  ├─script-phags-pa  
│  │          │  │  └─misc  
│  │          │  └─script-syriac  
│  │          │      └─misc  
│  │          ├─shaper-default  
│  │          │  ├─script-ethiopic  
│  │          │  │  └─misc  
│  │          │  ├─script-han  
│  │          │  │  └─misc  
│  │          │  ├─script-hiragana  
│  │          │  │  └─misc  
│  │          │  ├─script-linear-b  
│  │          │  │  └─misc  
│  │          │  └─script-tifinagh  
│  │          │      └─misc  
│  │          ├─shaper-hangul  
│  │          │  └─script-hangul  
│  │          │      └─misc  
│  │          ├─shaper-hebrew  
│  │          │  └─script-hebrew  
│  │          │      └─misc  
│  │          ├─shaper-indic  
│  │          │  ├─script-assamese  
│  │          │  │  └─utrrs  
│  │          │  │      ├─codepoint  
│  │          │  │      ├─gpos  
│  │          │  │      └─gsub  
│  │          │  ├─script-bengali  
│  │          │  │  ├─misc  
│  │          │  │  └─utrrs  
│  │          │  │      ├─codepoint  
│  │          │  │      ├─gpos  
│  │          │  │      └─gsub  
│  │          │  ├─script-devanagari  
│  │          │  │  ├─misc  
│  │          │  │  └─utrrs  
│  │          │  │      ├─codepoint  
│  │          │  │      ├─gpos  
│  │          │  │      └─gsub  
│  │          │  ├─script-gujarati  
│  │          │  │  └─utrrs  
│  │          │  │      ├─codepoint  
│  │          │  │      ├─gpos  
│  │          │  │      └─gsub  
│  │          │  ├─script-gurmukhi  
│  │          │  │  ├─misc  
│  │          │  │  └─utrrs  
│  │          │  │      ├─codepoint  
│  │          │  │      ├─gpos  
│  │          │  │      └─gsub  
│  │          │  ├─script-kannada  
│  │          │  │  ├─misc  
│  │          │  │  └─utrrs  
│  │          │  │      ├─codepoint  
│  │          │  │      ├─gpos  
│  │          │  │      └─gsub  
│  │          │  ├─script-malayalam  
│  │          │  │  ├─misc  
│  │          │  │  └─utrrs  
│  │          │  │      ├─codepoint  
│  │          │  │      └─gsub  
│  │          │  ├─script-oriya  
│  │          │  │  ├─misc  
│  │          │  │  └─utrrs  
│  │          │  │      ├─codepoint  
│  │          │  │      └─gsub  
│  │          │  ├─script-sinhala  
│  │          │  │  ├─misc  
│  │          │  │  └─utrrs  
│  │          │  │      ├─codepoint  
│  │          │  │      ├─gpos  
│  │          │  │      └─gsub  
│  │          │  ├─script-tamil  
│  │          │  │  ├─misc  
│  │          │  │  └─utrrs  
│  │          │  │      ├─codepoint  
│  │          │  │      ├─gpos  
│  │          │  │      └─gsub  
│  │          │  └─script-telugu  
│  │          │      ├─misc  
│  │          │      └─utrrs  
│  │          │          ├─codepoint  
│  │          │          ├─gpos  
│  │          │          └─gsub  
│  │          ├─shaper-khmer  
│  │          ├─shaper-myanmar  
│  │          │  └─script-myanmar  
│  │          │      └─misc  
│  │          ├─shaper-thai  
│  │          │  ├─script-lao  
│  │          │  │  └─misc  
│  │          │  └─script-thai  
│  │          │      └─misc  
│  │          ├─shaper-tibetan  
│  │          │  └─script-tibetan  
│  │          │      └─misc  
│  │          └─shaper-use  
│  │              ├─script-batak  
│  │              ├─script-buginese  
│  │              ├─script-cham  
│  │              ├─script-javanese  
│  │              ├─script-kaithi  
│  │              ├─script-kharoshti  
│  │              └─script-tai-tham  
│  └─subset  
│      └─data  
│          ├─expected  
│          │  ├─basics  
│          │  ├─cff-full-font  
│          │  ├─cff-japanese  
│          │  ├─full-font  
│          │  ├─japanese  
│          │  ├─layout  
│          │  └─layout.gpos  
│          ├─fonts  
│          ├─profiles  
│          └─tests  
└─util  
