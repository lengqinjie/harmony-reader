├─NuttX  
│  ├─drivers  
│  │  ├─bch  
│  │  ├─pipes  
│  │  └─video  
│  ├─fs  
│  │  ├─dirent  
│  │  ├─driver  
│  │  ├─inode  
│  │  ├─mount  
│  │  ├─nfs  
│  │  ├─tmpfs  
│  │  └─vfs  
│  └─include  
│      └─nuttx  
│          ├─fs  
│          ├─net  
│          ├─usb  
│          └─video  