└─zlib  
    ├─amiga  
    ├─contrib  
    │  ├─ada  
    │  ├─amd64  
    │  ├─asm686  
    │  ├─blast  
    │  ├─delphi  
    │  ├─dotzlib  
    │  │  └─DotZLib  
    │  ├─gcc_gvmat64  
    │  ├─infback9  
    │  ├─inflate86  
    │  ├─iostream  
    │  ├─iostream2  
    │  ├─iostream3  
    │  ├─masmx64  
    │  ├─masmx86  
    │  ├─minizip  
    │  ├─pascal  
    │  ├─puff  
    │  ├─testzlib  
    │  ├─untgz  
    │  └─vstudio  
    │      ├─vc10  
    │      ├─vc11  
    │      ├─vc12  
    │      ├─vc14  
    │      └─vc9  
    ├─doc  
    ├─examples  
    ├─msdos  
    ├─nintendods  
    ├─old  
    │  └─os2  
    ├─os400  
    ├─qnx  
    ├─test  
    ├─watcom  
    └─win32  