├─musl  
│  ├─arch  
│  │  ├─aarch64  
│  │  │  └─bits  
│  │  ├─arm  
│  │  │  └─bits  
│  │  ├─generic  
│  │  │  └─bits  
│  │  ├─i386  
│  │  │  └─bits  
│  │  ├─m68k  
│  │  │  └─bits  
│  │  ├─microblaze  
│  │  │  └─bits  
│  │  ├─mips  
│  │  │  └─bits  
│  │  ├─mips64  
│  │  │  └─bits  
│  │  ├─mipsn32  
│  │  │  └─bits  
│  │  ├─or1k  
│  │  │  └─bits  
│  │  ├─powerpc  
│  │  │  └─bits  
│  │  ├─powerpc64  
│  │  │  └─bits  
│  │  ├─riscv64  
│  │  │  └─bits  
│  │  ├─s390x  
│  │  │  └─bits  
│  │  ├─sh  
│  │  │  └─bits  
│  │  ├─x32  
│  │  │  └─bits  
│  │  └─x86_64  
│  │      └─bits  
│  ├─compat  
│  │  └─time32  
│  ├─crt  
│  │  ├─aarch64  
│  │  ├─arm  
│  │  ├─i386  
│  │  ├─microblaze  
│  │  ├─mips  
│  │  ├─mips64  
│  │  ├─mipsn32  
│  │  ├─or1k  
│  │  ├─powerpc  
│  │  ├─powerpc64  
│  │  ├─s390x  
│  │  ├─sh  
│  │  ├─x32  
│  │  └─x86_64  
│  ├─include  
│  │  ├─arpa  
│  │  ├─net  
│  │  ├─netinet  
│  │  ├─netpacket  
│  │  ├─scsi  
│  │  └─sys  
│  ├─kernel  
│  │  ├─arch  
│  │  │  ├─arm  
│  │  │  │  └─bits  
│  │  │  └─generic  
│  │  │      └─bits  
│  │  ├─include  
│  │  │  ├─arpa  
│  │  │  ├─net  
│  │  │  ├─netinet  
│  │  │  ├─netpacket  
│  │  │  └─sys  
│  │  ├─obj  
│  │  │  └─include  
│  │  │      └─bits  
│  │  └─src  
│  │      ├─ctype  
│  │      ├─dirent  
│  │      ├─env  
│  │      ├─errno  
│  │      ├─exit  
│  │      ├─fenv  
│  │      ├─include  
│  │      │  ├─arpa  
│  │      │  └─sys  
│  │      ├─internal  
│  │      ├─linux  
│  │      ├─locale  
│  │      ├─malloc  
│  │      ├─math  
│  │      │  └─arm  
│  │      ├─multibyte  
│  │      ├─network  
│  │      ├─prng  
│  │      ├─sched  
│  │      ├─stdio  
│  │      ├─stdlib  
│  │      ├─string  
│  │      └─time  
│  ├─ldso  
│  ├─src  
│  │  ├─aio  
│  │  ├─complex  
│  │  ├─conf  
│  │  ├─crypt  
│  │  ├─ctype  
│  │  ├─dirent  
│  │  ├─env  
│  │  ├─errno  
│  │  ├─exit  
│  │  │  └─arm  
│  │  ├─fcntl  
│  │  ├─fenv  
│  │  │  ├─aarch64  
│  │  │  ├─arm  
│  │  │  ├─i386  
│  │  │  ├─m68k  
│  │  │  ├─mips  
│  │  │  ├─mips64  
│  │  │  ├─mipsn32  
│  │  │  ├─powerpc  
│  │  │  ├─powerpc64  
│  │  │  ├─riscv64  
│  │  │  ├─s390x  
│  │  │  ├─sh  
│  │  │  ├─x32  
│  │  │  └─x86_64  
│  │  ├─include  
│  │  │  ├─arpa  
│  │  │  └─sys  
│  │  ├─internal  
│  │  │  ├─i386  
│  │  │  └─sh  
│  │  ├─ipc  
│  │  ├─ldso  
│  │  │  ├─aarch64  
│  │  │  ├─arm  
│  │  │  ├─i386  
│  │  │  ├─m68k  
│  │  │  ├─microblaze  
│  │  │  ├─mips  
│  │  │  ├─mips64  
│  │  │  ├─mipsn32  
│  │  │  ├─or1k  
│  │  │  ├─powerpc  
│  │  │  ├─powerpc64  
│  │  │  ├─riscv64  
│  │  │  ├─s390x  
│  │  │  ├─sh  
│  │  │  ├─x32  
│  │  │  └─x86_64  
│  │  ├─legacy  
│  │  ├─linux  
│  │  │  └─x32  
│  │  ├─locale  
│  │  ├─malloc  
│  │  ├─math  
│  │  │  ├─aarch64  
│  │  │  ├─arm  
│  │  │  ├─i386  
│  │  │  ├─mips  
│  │  │  ├─powerpc  
│  │  │  ├─powerpc64  
│  │  │  ├─riscv64  
│  │  │  ├─s390x  
│  │  │  ├─x32  
│  │  │  └─x86_64  
│  │  ├─misc  
│  │  ├─mman  
│  │  ├─mq  
│  │  ├─multibyte  
│  │  ├─network  
│  │  ├─passwd  
│  │  ├─prng  
│  │  ├─process  
│  │  │  ├─arm  
│  │  │  ├─i386  
│  │  │  ├─s390x  
│  │  │  ├─sh  
│  │  │  ├─x32  
│  │  │  └─x86_64  
│  │  ├─regex  
│  │  ├─sched  
│  │  ├─search  
│  │  ├─select  
│  │  ├─setjmp  
│  │  │  ├─aarch64  
│  │  │  ├─arm  
│  │  │  ├─i386  
│  │  │  ├─m68k  
│  │  │  ├─microblaze  
│  │  │  ├─mips  
│  │  │  ├─mips64  
│  │  │  ├─mipsn32  
│  │  │  ├─or1k  
│  │  │  ├─powerpc  
│  │  │  ├─powerpc64  
│  │  │  ├─riscv64  
│  │  │  ├─s390x  
│  │  │  ├─sh  
│  │  │  ├─x32  
│  │  │  └─x86_64  
│  │  ├─signal  
│  │  │  ├─aarch64  
│  │  │  ├─arm  
│  │  │  ├─i386  
│  │  │  ├─m68k  
│  │  │  ├─microblaze  
│  │  │  ├─mips  
│  │  │  ├─mips64  
│  │  │  ├─mipsn32  
│  │  │  ├─or1k  
│  │  │  ├─powerpc  
│  │  │  ├─powerpc64  
│  │  │  ├─riscv64  
│  │  │  ├─s390x  
│  │  │  ├─sh  
│  │  │  ├─x32  
│  │  │  └─x86_64  
│  │  ├─stat  
│  │  ├─stdio  
│  │  ├─stdlib  
│  │  ├─string  
│  │  │  ├─arm  
│  │  │  ├─i386  
│  │  │  └─x86_64  
│  │  ├─temp  
│  │  ├─termios  
│  │  ├─thread  
│  │  │  ├─aarch64  
│  │  │  ├─arm  
│  │  │  ├─i386  
│  │  │  ├─m68k  
│  │  │  ├─microblaze  
│  │  │  ├─mips  
│  │  │  ├─mips64  
│  │  │  ├─mipsn32  
│  │  │  ├─or1k  
│  │  │  ├─powerpc  
│  │  │  ├─powerpc64  
│  │  │  ├─riscv64  
│  │  │  ├─s390x  
│  │  │  ├─sh  
│  │  │  ├─x32  
│  │  │  └─x86_64  
│  │  ├─time  
│  │  └─unistd  
│  │      ├─mips  
│  │      ├─mips64  
│  │      ├─mipsn32  
│  │      ├─sh  
│  │      └─x32  
│  └─tools  