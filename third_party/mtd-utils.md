├─mtd-utils  
│  ├─include  
│  │  ├─linux  
│  │  └─mtd  
│  ├─jffsX-utils  
│  ├─lib  
│  ├─misc-utils  
│  ├─nand-utils  
│  ├─nor-utils  
│  ├─patches  
│  │  └─win-x86  
│  ├─tests  
│  │  ├─fs-tests  
│  │  │  ├─integrity  
│  │  │  ├─lib  
│  │  │  ├─simple  
│  │  │  ├─stress  
│  │  │  │  └─atoms  
│  │  │  └─utils  
│  │  ├─jittertest  
│  │  ├─mtd-tests  
│  │  ├─ubi-tests  
│  │  └─unittests  
│  │      └─sysfs_mock  
│  │          └─class  
│  │              ├─misc  
│  │              │  └─ubi_ctrl  
│  │              ├─mtd  
│  │              │  └─mtd0  
│  │              └─ubi  
│  ├─ubi-utils  
│  └─ubifs-utils  