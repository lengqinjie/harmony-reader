# HarmonyReader

#### 介绍
鸿蒙OS整体源码解读。协助大家学习和了解鸿蒙开源代码
[本仓库不再更新，请使用另外一个仓库](https://gitee.com/lengqinjie/openharmony-1.1.0)

#### 软件架构
以软件模块为单位。以实现文件(如.c文件，.cpp文件)为粒度对源代码进行解读
本仓库中会存储若干索引文件，每个文件会存储一些解读视频的超链接
索引文件的组织形式与源码目录保持一致

#### 安装教程

无

#### 使用说明

点击对应的索引文件，观看源码解读视频

#### 参与贡献

在观看视频中，如果发现错误，请及时反馈，可以在本仓库提交issue。
也欢迎大家录制视频并上传到互联网，然后从这里链接到你的视频

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
